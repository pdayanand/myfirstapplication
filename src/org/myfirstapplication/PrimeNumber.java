package org.myfirstapplication;

import java.util.stream.IntStream;

public class PrimeNumber {
    public static boolean checkIterate(int n) {
        if(n == 1 || n == 2)
            return false;
        for(int i = 3; i < n; i ++)
        {
            if(n%i == 0)
                return false;
        }
        return true;
    }

    public static boolean checkStream(int n) {
        return n> 2 && IntStream.range(3, n).noneMatch(i -> n % i == 0);
    }
}
