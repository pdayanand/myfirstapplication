package org.myfirstapplication;

public class Main {

    public static void main(String[] args) {
        FibonacciSeries.printIterate(46);
        FibonacciSeries.printStreamIterate(46);
        FibonacciSeries.printRecursive(10, 0 , 1);

        Palindrome.checkIterate("malayalam");
        Palindrome.checkIterate("blabla");

        Palindrome.checkStream("malayalam");

        Palindrome.checkStream("blabla");


        Palindrome.checkRecursive("malayalam");

        Palindrome.checkRecursive("blabla");

        System.out.println( PrimeNumber.checkIterate(7));
        System.out.println( PrimeNumber.checkIterate(5));
        System.out.println( PrimeNumber.checkIterate(1));
        System.out.println( PrimeNumber.checkIterate(100));
        System.out.println( PrimeNumber.checkIterate(2));
        System.out.println( PrimeNumber.checkStream(7));
        System.out.println( PrimeNumber.checkStream(5));
        System.out.println( PrimeNumber.checkStream(1));
        System.out.println( PrimeNumber.checkStream(100));
        System.out.println( PrimeNumber.checkStream(2));


        System.out.println(Factorial.generateIterate(5));
        System.out.println(Factorial.generateStream(5));

    }
}
