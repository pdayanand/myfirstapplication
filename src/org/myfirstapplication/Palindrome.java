package org.myfirstapplication;

import java.util.stream.IntStream;

public class Palindrome {

    public static void checkIterate(String input) {
        int stringlength = input.length();
        for(int i = 0; i <= stringlength/2; i++)
        {
            if(!(input.charAt(i) == input.charAt(stringlength-1-i))) {
                System.out.println("false");
                return;
            }

        }
        System.out.println("true");
    }

    public static void checkStream(String input) {
        System.out.println(IntStream.range(0, input.length()/2).allMatch(n -> input.charAt(n) == input.charAt(input.length()-1-n)));
    }

    public static void checkRecursive(String input) {
        System.out.println(checkRecursive(input, input.length() - 1));
    }

    public static boolean checkRecursive(String input, int count) {
        if(count > 0) {
            if(input.charAt(count) == input.charAt(input.length()-1-count))
                return checkRecursive(input, count - 1);
            else
                return false;
        }
        return true;
    }
}
