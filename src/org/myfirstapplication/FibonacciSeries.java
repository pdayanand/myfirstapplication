package org.myfirstapplication;

import java.util.stream.Stream;

public class FibonacciSeries {
    public static void printIterate(int n) {
        int first = 0;
        int second = 1;

        System.out.println(first);
        System.out.println(second);
        for (int i =0; i< n-2; i++) {
            int temp = second;
            second += first ;

            System.out.println(second);
            first = temp;
        }
    }

    public static void printStreamIterate(int n) {
        Stream.iterate(new int [] {0,1}, l->new int[] {l[1], l[0] + l[1]}).limit(n).forEach(l-> System.out.println(l[0]));
    }

    public static void printRecursive(int n, int first, int second) {

        if(n > 0)
        {
            System.out.println(first);
            printRecursive(n-1, second, second + first);
        }

    }


}
