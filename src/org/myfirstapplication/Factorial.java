package org.myfirstapplication;

import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class Factorial {
    public static int generateIterate(int n) {
        int product = 1;
        for(int i =1; i<=n; i++) {
            product *= i;
        }
        return product;
    }

    public static int generateStream (int n) {
        OptionalInt val = IntStream.range(1, n+1).reduce((i, j)-> i*j);
        return val.getAsInt();
    }
}
